{- |
Description :  Nearly-worthless poorly-written bot made as a Haskell learning project
Copyright   :  ©(c) Princess Riikka
License     :  http://opensource.org/licenses/BSD-3-Clause

Maintainer  :  PrincessRiikka@gmail.com
-}
module Main where
import Control.Monad
import Control.Concurrent
import Control.Applicative ((<$>), (<*>), (<*), (*>), (<$), pure)
import Control.Concurrent.Timer
import Control.Concurrent.Suspend.Lifted (mDelay, sDelay)
import Network.Fancy
import System.IO
import System.Environment
import System.Exit
--import System.Certificate.X509
import System.Random
import Data.Maybe (fromMaybe)
import Data.Text (strip, pack, unpack)
import Data.List (isPrefixOf)
import qualified Data.Text as T
import Graphics.Vty.Widgets.All
import Graphics.Vty hiding (Button)
import IRCProto

--TODO:
-- Fix the bloody parsing
-- Make send thread exit nicely
-- \mango on a timer somehow; \quit from authed users; more ideas?
-- handlePM looks wrong (so many 'arguments'). Is there a better way?
-- Add a way to actually auth users :D
-- Split things out into other files. This one is messy.
-- Implement user priv/mode tracking

getRandomBlurb :: String -> IO String
getRandomBlurb blurbFile = do
  text <- lines <$> readFile blurbFile
  loc <- randomRIO (0, length text)
  return (text !! (loc-1))

printToLog :: String -> IO ()
printToLog str = appendFile "Loggie-Kin" (str ++ "\n")

sendStrImpl :: Handle -> MVar String -> (String -> IO ()) -> IO ()
sendStrImpl hnd var output = forever $ do
  str <- takeMVar var
  output $ ">>" ++ stripString str --debug? Maybe just leave it to show bot output
  hPutStr hnd str

sendStr :: MVar String -> String -> IO ()
sendStr var str = putMVar var (str ++ "\r\n")

sendToChannel :: (String -> IO ()) -> String -> String -> IO ()
sendToChannel send chan msg = send ("PRIVMSG " ++ chan ++ " :" ++ msg)

doHandshake :: (String -> IO ()) -> String -> IO ()
doHandshake send "" = send "NICK BottieKin" >> send "USER BottieKin 0 *: PrincessRiikka"
doHandshake send pass = send ("PASS :" ++ pass) >> doHandshake send  ""

doScience :: IO [String]
doScience = putStrLn "Connection closed." >> exitWith (ExitFailure 1)

shouldWeQuit :: [String] -> Bool
shouldWeQuit [] = False
shouldWeQuit [" !!quit"] = True

startHandleConnection :: (String -> IO ()) -> (String -> IO ()) -> Handle -> MVar Bool -> [String] -> IO [String]
startHandleConnection send output hnd blockVar pLine =
  if shouldWeQuit pLine
    then
      putMVar blockVar True >> return []
    else handleConnection send output hnd pLine >>= startHandleConnection send output hnd blockVar

handlePM :: User -> String -> String -> (String -> IO ()) -> [String] -> IO [String]
handlePM user chan msg send authedUsers
  | stripString msg == "\\mango" = sendToChannel send chan "Please, no more mangoes :(" >> return authedUsers
  | stripString msg == "\\quit" = if elem (name user) authedUsers then return [" !!quit"] else return authedUsers
  | otherwise = printToLog msg >> return authedUsers

handleResult :: (String -> IO ()) -> (String -> IO ()) -> [String] -> Message -> IO [String]
handleResult send output authedUsers msg = do
  output $ show msg
  case msg of
    Ping str -> send ("PONG :"++str) >> return authedUsers
    PrivMsg user chan msg -> handlePM user chan msg send authedUsers
    otherwise -> return authedUsers

handleConnection :: (String -> IO ()) -> (String -> IO ()) -> Handle -> [String] -> IO [String]
handleConnection send output handle authedUsers = do
  stillAlive <- hIsEOF handle --Misleading but amusing. <.< :3
  if stillAlive
    then doScience
    else do
      hWaitForInput handle (-1)
      line <- hGetLine handle
      printToLog line --log the literal input from IRC
      let parseResult = parseMessage line
      handleResult send output authedUsers parseResult

parseArgs :: [String] -> [String]
parseArgs args
  | length args == 2 = args ++ [""]
  | length args == 3 = args
  | otherwise = []

joinChannel :: (String -> IO ()) -> String -> IO ()
joinChannel send chan =
  send $ "JOIN " ++ chan

partChannel :: (String -> IO ()) -> String -> String -> IO ()
partChannel send chan msg =
  send $ "PART " ++ chan ++ " :" ++ msg ++ ""

doBlurbs :: (String -> IO ()) -> IO TimerIO
doBlurbs send = do
  blurb <- getRandomBlurb "blurbs"
  --send ("PRIVMSG #chan :" ++ blurb)
  time <- randomRIO (280, 650)
  oneShotTimer (void $ doBlurbs send) (sDelay time)

applyText :: String -> Widget Edit -> IO ()
applyText [] _ = return ()
applyText str wid = applyEdit (insertChar $ head str) wid >> applyText (tail str) wid

outputImpl :: Widget Edit -> String -> IO ()
outputImpl outPane tex = do
  outText <- getEditText outPane
  setEditText outPane $ T.append outText (T.pack $ tex ++ "\n\n")
  setEditCursorPosition (length (T.lines outText)+1,0) outPane
  return ()

outputImpl' :: Widget (List T.Text FormattedText) -> String -> IO ()
outputImpl' outPane tex = do
  let texLines = lines tex
  unless (null texLines) $ do
    let headTex = head texLines
    let outTex = if length headTex < 80 then [headTex] else [take 80 headTex, drop 80 headTex]
    addToList outPane (T.pack $ head outTex) =<< plainText (T.pack $ head outTex)
    scrollToEnd outPane
    when (length outTex > 1) $ outputImpl' outPane (concat $ tail outTex)
    when (length texLines > 1) $ outputImpl' outPane (concat $ tail texLines)

handleOutput :: Widget (List T.Text FormattedText) -> String -> IO ()
handleOutput outPane tex = schedule $ outputImpl' outPane tex

handleInput :: (String -> IO ()) -> Widget Edit -> IO ()
handleInput send wid = do
  inp <- getEditText wid
  let input = T.unpack inp
  setEditText wid $ T.pack ""
  unless (null input) $
    case head $ words input of
      "/join" -> joinChannel send (head.tail $ words input)
      "/part" -> if length (words input) > 2 then
        partChannel send (head.tail $ words input) (unwords $ tail.tail $ words input)
        else partChannel send (head.tail $ words input) ""
      "/say" -> sendToChannel send (words input !! 1) (unwords $ drop 2 $ words input)
      otherwise -> return ()

setupConnHandle connHandle = do
  hSetBuffering connHandle LineBuffering
  encoding <- mkTextEncoding "UTF-8 //TRANSLIT"
  hSetEncoding connHandle encoding

runTUI bawks outPane inputLine tb send = do
  setBoxChildSizePolicy bawks (PerChild BoxAuto (BoxFixed 1))
  setBoxSpacing bawks 1
  setSelectedFocusedAttr outPane (Just (withBackColor defAttr black))
  setSelectedUnfocusedAttr outPane (Just (withBackColor defAttr black))

  fg <- newFocusGroup
  addToFocusGroup fg inputLine
  addToFocusGroup fg tb

  coll <- newCollection
  addToCollection coll bawks fg

  onActivate inputLine (handleInput send)

  fg `onKeyPressed` \_ k _ ->
    case k of
      KEsc -> shutdownUi >> return True
      _ -> return False

  runUi coll defaultContext { skin = unicodeRoundedSkin }
  
main = do
    args <- getArgs
    let connectTo = parseArgs args
    case length connectTo of
      3 -> do
        let host = head connectTo --connectTo !! 0 --Now it looks weird. Thanks, hlint :|
        let port = connectTo !! 1
        let pass = connectTo !! 2
        --(TUI) We need these here so we can use them for output
        outPane <- newTextList [] 1
        inputLine <- editWidget
        tb <- bordered outPane
        bawks <- vBox tb inputLine
        --(/TUI)
        connHandle <- connectStream $ IP host $ read port
        setupConnHandle connHandle
        sendVar <- newEmptyMVar
        --IO helpers
        let output = handleOutput outPane
        let send = sendStr sendVar
        forkIO $ void (sendStrImpl connHandle sendVar output)

        doHandshake send pass
        joinChannel send "#princessriikka"

        blockVar <- newEmptyMVar
        forkIO $ void (startHandleConnection send output connHandle blockVar [])
        --blurbTimer <- oneShotTimer (void $ doBlurbs send) (sDelay 60)

        --TUI stuff
        runTUI bawks outPane inputLine tb send
        --End TUI stuff
        putStrLn "END"
      _ -> exitWith(ExitFailure 1)
